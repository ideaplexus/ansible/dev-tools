# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.3.0] - 2024-07-30
### Added
- Added `go.test.flags` to `go_test`
- Read environment variable `ANSIBLE_CONTAINER_ENABLED` and override: `container.enabled` variable, if available

### Changed
- Removed unused GO_VERSION arg from ssh-container.
- Using consequent `tee` instead of `>`.
- Changed required ansible version to `>=2.15.0`.
- Changed the url of oapi-codegen to the new location.
- Updated README.md

### Fixed
- Ensure dev-container-* is (re-)build with `nocache` option to ensure an updated container state.

## [1.2.0] - 2024-06-17
### Fixed
- Fixed way to detect if container (role) is enabled
- Fixed dependencies, issues with requests >= 2.32.0 and docker

## [1.1.1] - 2024-05-22
### Changed
- added -trimpath to for `go:build` command

### Fixed
- Fixed an issue with dependencies
- Fixed stat source directories for `go:test`

## [1.1.0] - 2024-05-01
### Added
- Support for "." (base directory) directory for `go:test`

### Fixed
- Search only for `main.go` files for `go:build`

## [1.0.0] - 2024-04-25
### Added
- Added alias `vendor` and `vendor:upgrade` to `go:vendor` and `go:vendor:upgrade`

### Fixed
- Read go version
- Run go tests tasks only if src dirs exists/set

## [0.9.2] - 2023-11-09
### Added
- Add ssh/git config for gitlab.com & github.com to access private projects

## [0.9.1] - 2023-11-07
### Fixed
- Wrong version in `galaxy.yml`

## [0.9.0] - 2023-11-07
### Changed
- Reworked `go_build` role again to support `environment` and `flags` per file

## [0.8.0] - 2023-11-07
### Changed
- Reworked `go_build` role to support new project type `lambda/provided.al2`

[Unreleased]: https://gitlab.com/ideaplexus/ansible-dev-tools/-/compare/1.3.0...main
[1.3.0]: https://gitlab.com/ideaplexus/ansible-dev-tools/-/compare/1.2.0...1.3.0
[1.2.0]: https://gitlab.com/ideaplexus/ansible-dev-tools/-/compare/1.1.1...1.2.0
[1.1.1]: https://gitlab.com/ideaplexus/ansible-dev-tools/-/compare/1.1.0...1.1.1
[1.1.0]: https://gitlab.com/ideaplexus/ansible-dev-tools/-/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/ideaplexus/ansible-dev-tools/-/compare/0.9.2...1.0.0
[0.9.2]: https://gitlab.com/ideaplexus/ansible-dev-tools/-/compare/0.9.1...0.9.2
[0.9.1]: https://gitlab.com/ideaplexus/ansible-dev-tools/-/compare/0.9.0...0.9.1
[0.9.0]: https://gitlab.com/ideaplexus/ansible-dev-tools/-/compare/0.8.0...0.9.0
[0.8.0]: https://gitlab.com/ideaplexus/ansible-dev-tools/-/tags/0.8.0
