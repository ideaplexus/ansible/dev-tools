# code: language=ansible
---
- name: Install goverreport
  delegate_to: "{{ task_host }}"
  ansible.builtin.command:
    chdir: "{{ base_dir }}"
    cmd: go install github.com/mcubik/goverreport@{{ test_vars.goverreport.version }}
  register: goverreport_install
  changed_when: goverreport_install.stderr_lines | length > 0
  tags:
    - test
    - go:all
    - go:test

- name: Install gocover-cobertura
  delegate_to: "{{ task_host }}"
  ansible.builtin.command:
    chdir: "{{ base_dir }}"
    cmd: go install github.com/boumenot/gocover-cobertura@{{ test_vars.gocover_cobertura.version }}
  register: gocover_cobertura_install
  when:
    - ci
  changed_when: gocover_cobertura_install.stderr_lines | length > 0
  tags:
    - test
    - go:all
    - go:test

- name: Stat source directories
  ansible.builtin.stat:
    path: "{{ item }}"
  with_items: "{{ test_vars.src_dirs }}"
  register: go_source_stat
  tags:
    - test
    - go:all
    - go:test

- name: Create source directories string
  ansible.builtin.set_fact:
    go_test_dirs: "{{ go_source_stat | json_query('results[*].stat | [?exists].path') | list | map('regex_replace', '^([^\\.].+)$', './\\1...') | join(' ') }}"
  tags:
    - test
    - go:all
    - go:test

- name: Run go test with coverprofile output
  delegate_to: "{{ task_host }}"
  ansible.builtin.command:
    chdir: "{{ base_dir }}"
    cmd: "go test {{ test_vars.flags | join(' ') }} -race -coverprofile cp.out {{ go_test_dirs }}"
  register: go_test_coverprofile_result
  changed_when: go_test_coverprofile_result.rc == 0
  ignore_errors: true
  when:
    - go_test_dirs | length > 0
  tags:
    - test
    - go:all
    - go:test

- name: Run go test with json report output
  delegate_to: "{{ task_host }}"
  ansible.builtin.command:
    chdir: "{{ base_dir }}"
    cmd: "go test {{ test_vars.flags | join(' ') }} -v -json {{ go_test_dirs }}"
  register: go_test_json_report_result
  changed_when: go_test_json_report_result.stdout_lines | length > 0
  ignore_errors: true
  when:
    - ci
    - go_test_dirs | length > 0
  tags:
    - test
    - go:all
    - go:test

- name: Write go test json report output to file
  delegate_to: "{{ task_host }}"
  ansible.builtin.copy:
    dest: "{{ base_dir }}tr.out"
    mode: "644"
    content: "{{ go_test_json_report_result.stdout }}"
  when:
    - ci
    - go_test_dirs | length > 0
  tags:
    - test
    - go:all
    - go:test

- name: Remove generated files from coverprofile output
  delegate_to: "{{ task_host }}"
  ansible.builtin.lineinfile:
    path: "{{ base_dir ~ item }}"
    state: absent
    regexp: (gen|pb)\.go
  with_items:
    - cp.out
  when:
    - go_test_dirs | length > 0
  tags:
    - test
    - go:all
    - go:test

- name: Run goverreport
  delegate_to: "{{ task_host }}"
  ansible.builtin.command:
    chdir: "{{ base_dir }}"
    cmd: goverreport -coverprofile cp.out
  register: goverreport_result
  changed_when: goverreport_result.rc == 0
  when:
    - go_test_dirs | length > 0
  tags:
    - test
    - go:all
    - go:test

- name: Remove coverprofile output
  delegate_to: "{{ task_host }}"
  ansible.builtin.file:
    path: "{{ base_dir ~ item }}"
    state: absent
  with_items:
    - cp.out
  when:
    - not ci
    - go_test_dirs | length > 0
  tags:
    - test
    - go:all
    - go:test

- name: Run go tool cover
  delegate_to: "{{ task_host }}"
  ansible.builtin.command:
    chdir: "{{ base_dir }}"
    cmd: go tool cover -func cp.out
  register: go_tool_cover_result
  when:
    - ci
    - go_test_dirs | length > 0
  changed_when: go_tool_cover_result.rc == 0
  tags:
    - test
    - go:all
    - go:test

- name: Generate Go Coverage Report html
  delegate_to: "{{ task_host }}"
  ansible.builtin.command:
    chdir: "{{ base_dir }}"
    cmd: go tool cover -html cp.out -o cp.html
  register: go_tool_cover_html_result
  when:
    - ci
    - go_test_dirs | length > 0
  changed_when: go_tool_cover_html_result.rc == 0
  tags:
    - test
    - go:all
    - go:test

- name: Generate cobertura output from coverprofile
  delegate_to: "{{ task_host }}"
  ansible.builtin.shell:
    chdir: "{{ base_dir }}"
    cmd: gocover-cobertura < cp.out > co.out
  register: gocover_cobertura_xml_result
  when:
    - ci
    - go_test_dirs | length > 0
  changed_when: gocover_cobertura_xml_result.rc == 0
  tags:
    - test
    - go:all
    - go:test

- name: Show coverprofile
  ansible.builtin.debug:
    var: go_test_coverprofile_result.stdout
  when:
    - ci
    - go_test_dirs | length > 0
  tags:
    - test
    - go:all
    - go:test

- name: Show goverreport
  ansible.builtin.debug:
    var: goverreport_result.stdout
  when:
    - go_test_dirs | length > 0
  tags:
    - test
    - go:all
    - go:test

- name: Show go tool cover
  ansible.builtin.debug:
    var: go_tool_cover_result.stdout
  when:
    - ci
    - go_test_dirs | length > 0
  tags:
    - test
    - go:all
    - go:test

- name: Test failed
  ansible.builtin.fail:
    msg: Tests failed.
  when:
    - go_test_dirs | length > 0
    - go_test_coverprofile_result.rc != 0
  tags:
    - test
    - go:all
    - go:test

- name: No tests found
  ansible.builtin.debug:
    msg: "No tests were found"
  when:
    - go_test_dirs | length == 0
  tags:
    - test
    - go:all
    - go:test
