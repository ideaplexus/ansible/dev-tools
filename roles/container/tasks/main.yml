# code: language=ansible
---
- name: Force container rebuild detected
  ansible.builtin.set_fact:
    container_rebuild: true
  tags:
    - never
    - container:rebuild
    - container-ssh:rebuild
    - container-dev:rebuild
  when: container_enabled
  changed_when: 'true'

- name: Remove image for 'dev-container-ssh'
  community.docker.docker_image:
    state: absent
    name: dev-container-ssh
    force_absent: true
  tags:
    - never
    - container:rebuild
    - container-ssh:rebuild
  when: container_enabled

- name: Remove image for 'dev-container-{{ project.name }}'
  community.docker.docker_image:
    state: absent
    name: dev-container-{{ project.name }}
    force_absent: true
  tags:
    - never
    - container:rebuild
    - container-dev:rebuild
  when: container_enabled

- name: Stop 'dev-container-ssh'
  community.docker.docker_container:
    state: absent
    name: dev-container-ssh
  tags:
    - never
    - container:rebuild
    - container-dev:rebuild
  when: container_enabled
  changed_when: 'true'

- name: Stop 'dev-container-{{ project.name }}'
  community.docker.docker_container:
    state: absent
    name: dev-container-{{ project.name }}
  tags:
    - never
    - container:rebuild
    - container-dev:rebuild
  when: container_enabled
  changed_when: 'true'

- name: Get state of 'dev-container-ssh'
  community.docker.docker_container_info:
    name: dev-container-ssh
  register: docker_info_ssh_initial
  tags:
    - always
  when: container_enabled
  changed_when: 'true'

- name: Get state of 'dev-container-{{ project.name }}'
  community.docker.docker_container_info:
    name: dev-container-{{ project.name }}
  register: docker_info_dev_initial
  tags:
    - always
  when: container_enabled
  changed_when: 'true'

- name: Add local ssh identity
  ansible.builtin.shell:
    cmd: set -o pipefail && ssh-add --apple-use-keychain ~/.ssh/id_rsa
  args:
    executable: /bin/bash
  tags:
    - always
  when:
    - container_enabled
    - ansible_facts["os_family"] == "Darwin"
    - not docker_info_ssh_initial.exists or not docker_info_dev_initial.exists
    - not (docker_info_ssh_initial.container.State.Running | default(false)) or not (docker_info_dev_initial.container.State.Running | default(false))
  changed_when: 'true'

- name: Get local ssh identity
  ansible.builtin.shell:
    cmd: set -o pipefail && ssh-add -L | base64 | tr -d '\n'
  args:
    executable: /bin/bash
  register: authorized_keys
  changed_when: authorized_keys | length > 0
  tags:
    - always
  when:
    - container_enabled
    - not docker_info_ssh_initial.exists or not docker_info_dev_initial.exists
    - not (docker_info_ssh_initial.container.State.Running | default(false)) or not (docker_info_dev_initial.container.State.Running | default(false))

- name: Ensure shared volume 'dev-volume-ssh' exists
  community.docker.docker_volume:
    name: dev-volume-ssh
    state: present
  tags:
    - always
    - container-ssh
    - container-ssh:rebuild
    - container:rebuild
  when:
    - container_enabled
    - not docker_info_ssh_initial.exists or not docker_info_dev_initial.exists
    - not (docker_info_ssh_initial.container.State.Running | default(false)) or not (docker_info_dev_initial.container.State.Running | default(false))

- name: Build 'dev-container-ssh'
  community.docker.docker_image:
    source: build
    build:
      path: "{{ role_path }}/files/ssh"
      args:
        SSH_PORT: "{{ container_vars.ssh_port }}"
      pull: true
    name: dev-container-ssh
    state: present
    push: false
  tags:
    - container-ssh
    - container-ssh:rebuild
    - container:rebuild
  when:
    - container_enabled
    - container_rebuild | default(false)


- name: Start 'dev-container-ssh'
  community.docker.docker_container:
    name: dev-container-ssh
    image: dev-container-ssh
    state: started
    auto_remove: true
    restart: true
    volumes: dev-volume-ssh:/ssh/socket
    ports: "{{ (ansible_facts['os_family'] == 'Darwin' or ansible_facts['virtualization_type'] == 'wsl') | ternary(['0:' ~ container_vars.ssh_port], omit) }}"
    env:
      AUTHORIZED_KEYS: "{{ authorized_keys.stdout }}"
  tags:
    - always
    - container-ssh
    - container-ssh:rebuild
    - container:rebuild
  when:
    - container_enabled
    - not (docker_info_ssh_initial.container.State.Running | default(false))

- name: Get state of 'dev-container-ssh'
  community.docker.docker_container_info:
    name: dev-container-ssh
  register: docker_info_ssh_started
  tags:
    - always
    - container-ssh
    - container-ssh:rebuild
    - container:rebuild
  when: container_enabled

- name: Set host of 'dev-container-ssh'
  ansible.builtin.set_fact:
    # yamllint disable-line rule:line-length
    ssh_container_ssh_host: "{{ (ansible_facts['os_family'] == 'Darwin' or ansible_facts['virtualization_type'] == 'wsl') | ternary('127.0.0.1', docker_info_ssh_started.container.NetworkSettings.IPAddress) }}"
  tags:
    - always
    - container-ssh
    - container-ssh:rebuild
    - container:rebuild
  when: container_enabled
  changed_when: 'true'

- name: Set port of 'dev-container-ssh'
  ansible.builtin.set_fact:
    # yamllint disable-line rule:line-length
    ssh_container_ssh_port: "{{ (ansible_facts['os_family'] == 'Darwin' or ansible_facts['virtualization_type'] == 'wsl') | ternary(docker_info_ssh_started.container.NetworkSettings.Ports[container_vars.ssh_port ~ '/tcp'][0].HostPort, container_vars.ssh_port) }}"
  tags:
    - always
    - container-ssh
    - container-ssh:rebuild
    - container:rebuild
  when: container_enabled
  changed_when: 'true'

- name: Add hostname of 'dev-container-ssh' to inventory to store host & port
  ansible.builtin.add_host:
    name: dev-container-ssh
    host: "{{ ssh_container_ssh_host }}"
    port: "{{ ssh_container_ssh_port }}"
  when:
    - container_enabled
    - not (docker_info_ssh_initial.container.State.Running | default(false))

- name: Wait until ssh inside 'dev-container-ssh' is up
  ansible.builtin.wait_for:
    host: "{{ ssh_container_ssh_host }}"
    port: "{{ ssh_container_ssh_port | int }}"
    delay: 1
    timeout: 10
  tags:
    - container-ssh
    - container-ssh:rebuild
    - container:rebuild
  when:
    - container_enabled
    - not (docker_info_ssh_initial.container.State.Running | default(false))

- name: Gather public ssh key for 'dev-container-ssh'
  ansible.builtin.shell:
    cmd: >
      ssh-keyscan
      -p {{ ssh_container_ssh_port }}
      {{ ssh_container_ssh_host }}
      2> /dev/null
  register: dev_container_ssh_public_keys
  changed_when: dev_container_ssh_public_keys | length > 0
  tags:
    - container-ssh
    - container-ssh:rebuild
    - container:rebuild
  when:
    - container_enabled
    - not (docker_info_ssh_initial.container.State.Running | default(false))

- name: Create temporary known_hosts file
  ansible.builtin.tempfile:
    state: file
    prefix: dev-container-ssh.
  register: temporary_known_hosts_file
  tags:
    - container-ssh
    - container-ssh:rebuild
    - container:rebuild
  when:
    - container_enabled
    - not (docker_info_ssh_initial.container.State.Running | default(false))

- name: Write public ssh key to temporary known_hosts file
  ansible.builtin.copy:
    content: "{{ dev_container_ssh_public_keys.stdout }}"
    dest: "{{ temporary_known_hosts_file.path }}"
    mode: "644"
  tags:
    - container-ssh
    - container-ssh:rebuild
    - container:rebuild
  when:
    - container_enabled
    - not (docker_info_ssh_initial.container.State.Running | default(false))

- name: Start ssh-agent inside 'dev-container-ssh'
  ansible.builtin.shell:
    cmd: >
      ssh
      -A
      -f
      -o 'UserKnownHostsFile={{ temporary_known_hosts_file.path }}'
      -p {{ ssh_container_ssh_port }}
      -S none
      'root@{{ ssh_container_ssh_host }}'
      /ssh/agent.sh
  tags:
    - container-ssh
    - container-ssh:rebuild
    - container:rebuild
  when:
    - container_enabled
    - not (docker_info_ssh_initial.container.State.Running | default(false))
  changed_when: 'true'

- name: Create temporary Dockerfile file for 'dev-container-{{ project.name }}'
  ansible.builtin.tempfile:
    state: file
    prefix: dev-container-dockerfile.
  register: dev_container_dockerfile
  tags:
    - container-dev
    - container-dev:rebuild
    - container:rebuild
  when:
    - container_enabled
    - (container_rebuild | default(false)) or not docker_info_dev_initial.exists

- name: Render Dockerfile for 'dev-container-{{ project.name }}'
  ansible.builtin.template:
    src: "{{ container_vars.dev.dockerfile_template }}"
    dest: "{{ dev_container_dockerfile.path }}"
    mode: "644"
  tags:
    - container-dev
    - container-dev:rebuild
    - container:rebuild
  when:
    - container_enabled
    - (container_rebuild | default(false)) or not docker_info_dev_initial.exists

- name: Build 'dev-container-{{ project.name }}'
  community.docker.docker_image:
    source: build
    build:
      dockerfile: "{{ dev_container_dockerfile.path }}"
      path: "{{ role_path }}/files/dev"
      pull: true
      nocache: true
    name: dev-container-{{ project.name }}
    state: present
    push: false
  tags:
    - container-dev
    - container-dev:rebuild
    - container:rebuild
  when:
    - container_enabled
    - (container_rebuild | default(false)) or not docker_info_dev_initial.exists

- name: Start 'dev-container-{{ project.name }}'
  community.docker.docker_container:
    name: dev-container-{{ project.name }}
    image: dev-container-{{ project.name }}
    state: started
    auto_remove: true
    restart: true
    volumes:
      - "dev-volume-ssh:/ssh/socket"
      - "{{ playbook_dir }}:/home/user/project"
    ports: "{{ (ansible_facts['os_family'] == 'Darwin' or ansible_facts['virtualization_type'] == 'wsl') | ternary(['0:' ~ container_vars.ssh_port], omit) }}"
    env:
      AUTHORIZED_KEYS: "{{ authorized_keys.stdout | default('') }}"
  tags:
    - always
    - container-dev
    - container-dev:rebuild
    - container:rebuild
  when:
    - container_enabled
    - not (docker_info_dev_initial.container.State.Running | default(false))

- name: Get state of 'dev-container-{{ project.name }}'
  community.docker.docker_container_info:
    name: dev-container-{{ project.name }}
  register: docker_info_dev_started
  tags:
    - always
    - container-dev
    - container-dev:rebuild
    - container:rebuild
  when:
    - container_enabled

- name: Set host of 'dev-container-{{ project.name }}'
  ansible.builtin.set_fact:
    # yamllint disable-line rule:line-length
    dev_container_ssh_host: "{{ (ansible_facts['os_family'] == 'Darwin' or ansible_facts['virtualization_type'] == 'wsl') | ternary(ansible_facts['nodename'], docker_info_dev_started.container.NetworkSettings.IPAddress) }}"
  tags:
    - always
    - container-ssh
    - container-ssh:rebuild
    - container:rebuild
  when: container_enabled
  changed_when: 'true'

- name: Set port of 'dev-container-{{ project.name }}'
  ansible.builtin.set_fact:
    # yamllint disable-line rule:line-length
    dev_container_ssh_port: "{{ (ansible_facts['os_family'] == 'Darwin' or ansible_facts['virtualization_type'] == 'wsl') | ternary(docker_info_dev_started.container.NetworkSettings.Ports[container_vars.ssh_port ~ '/tcp'][0].HostPort, container_vars.ssh_port) }}"
  tags:
    - always
    - container-ssh
    - container-ssh:rebuild
    - container:rebuild
  when: container_enabled
  changed_when: 'true'

- name: Add host & port of 'dev-container-{{ project.name }}' to inventory
  ansible.builtin.add_host:
    name: "{{ dev_container_ssh_host }}:{{ dev_container_ssh_port }}"
  tags:
    - always
    - container-dev
    - container-dev:rebuild
    - container:rebuild
  when:
    - container_enabled

- name: Set 'task_host' to 'dev-container-{{ project.name }}'
  ansible.builtin.set_fact:
    task_host: "{{ dev_container_ssh_host }}"
  tags:
    - always
    - container-dev
    - container-dev:rebuild
    - container:rebuild
  changed_when: 'true'
  when:
    - container_enabled

- name: Wait until ssh inside 'dev-container-{{ project.name }}' is up
  ansible.builtin.wait_for:
    host: "{{ dev_container_ssh_host }}"
    port: "{{ dev_container_ssh_port | int }}"
    delay: 1
    timeout: 10
  tags:
    - always
    - container-dev
    - container-dev:rebuild
    - container:rebuild
  when:
    - container_enabled
