FROM alpine:latest

ARG SSH_PORT=22

ENV SSH_AUTH_SOCK=/ssh/socket/agent.sock

RUN set -eux \
  && apk upgrade --no-cache \
  && apk add --no-cache \
    bash \
    openssh \
    socat \
    supervisor \
    tini

RUN set -eux \
  && ssh-keygen -A \
  && echo -e "password\npassword" | passwd \
  && sed -i "s/#Port 22/Port ${SSH_PORT}/g" /etc/ssh/sshd_config \
  && sed -i "s/#PasswordAuthentication yes/PasswordAuthentication no/g" /etc/ssh/sshd_config \
  && sed -i "s/#StrictModes yes/StrictModes no/g" /etc/ssh/sshd_config \
  && sed -i "s/AuthorizedKeysFile.*/AuthorizedKeysFile \/etc\/ssh\/authorized_keys/g" /etc/ssh/sshd_config

COPY entrypoint.sh /ssh/entrypoint.sh
COPY agent.sh /ssh/agent.sh
COPY supervisord.conf /etc/supervisord.conf

WORKDIR /ssh

EXPOSE ${SSH_PORT}

ENTRYPOINT ["/sbin/tini", "--", "/ssh/entrypoint.sh"]

CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]
