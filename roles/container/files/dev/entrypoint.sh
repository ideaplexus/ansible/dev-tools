#!/usr/bin/env bash

set -eo pipefail

echo "${AUTHORIZED_KEYS}" | base64 -d > /etc/ssh/authorized_keys

exec "$@"
