# Ansible Dev Tools

- [Requirements](#requirements)
  - [Ubuntu/Linux](#ubuntulinux)
    - [Docker](#docker)
    - [Ansible on Ubuntu](#ansible-on-ubuntu)
  - [macOS](#macos)
    - [Xcode Command Line Tools](#xcode-command-line-tools)
    - [Docker Desktop on Mac](#docker-desktop-on-mac)
    - [Ansible on Mac](#ansible-on-mac)
  - [Windows](#windows)
    - [Install WSL/Ubuntu](#install-wslubuntu)
    - [Docker Desktop for Windows](#docker-desktop-for-windows)
    - [Keychain](#keychain)
  - [All OS](#all-os)
    - [SSH](#ssh)
    - [Git](#git)
- [Setup on your own project](#setup-on-your-own-project)
  - [*ansible.cfg* - Ansible configuration](#ansiblecfg-ansible-configuration)
  - [*ansible.requirements.yml* - Ansible playbook dependencies](#ansiblerequirementsyml-ansible-playbook-dependencies)
  - [*ansible.dev.yml* - Ansible playbook](#ansibledevyml-ansible-playbook)
  - [*ansible.ci.yml* - Ansible playbook - use with gitlab ci pipeline](#ansibleciyml-ansible-playbook-use-with-gitlab-ci-pipeline)
  - [*Makefile* - Shortcuts for most common tags](#makefile-shortcuts-for-most-common-tags)
- [Usage](#usage)
- [Advanced usage](#advanced-usage)
  - [Roles](#roles)
    - [`common`](#common)
    - [`container`](#container)
    - [`container_ssh`](#container_ssh)
    - [`go`](#go)
    - [`go_clean`](#go_clean)
    - [`go_vendor`](#go_vendor)
    - [`go_oapi_codegen`](#go_oapi_codegen)
      - [Example configurations](#example-configurations)
    - [`go_mockery`](#go_mockery)
    - [`go_generate`](#go_generate)
    - [`go_golangci_lint`](#go_golangci_lint)
    - [`go_test`](#go_test)
    - [`go_build`](#go_build)
    - [`buf`](#buf)
    - [`vacuum`](#vacuum)
- [Advanced examples](#advanced-examples)
  - [Upgrade projects go version](#upgrade-projects-go-version)

## Requirements

---

*Note*

Keep your operating system always up-to-date!

---

### Ubuntu/Linux

Install common dependencies

```shell
sudo apt-get -y install git make
```

#### Docker

Add Docker's official GPG key

```shell
sudo apt-get update
sudo apt-get -y install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
```

Add the repository to Apt sources

```shell
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```

Install `docker` including plugins

```shell
sudo apt-get -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

Create a docker group and add yourself to this group

```shell
sudo groupadd docker
sudo usermod -aG docker $USER
```

---

*Note*

You have to re-login apply the group changes!

---

#### Ansible on Ubuntu

Install `pip3` with the following command

```shell
sudo apt-get -y install python3-pip
```

Based on your shell, replace the `$YOUR_RC_FILE` with the name of your rc file (e.g. `.bashrc` or `.zshrc`) file located in your home directory
and run the following command

```shell
echo "export PATH=\$PATH:$(python3 -m site --user-base)/bin" | tee -a ~/$YOUR_RC_FILE
```

---

*Note*

You have to re-login apply the changes to your .*rc file permanently!

---

Install `ansible` via pip

```shell
pip3 install ansible
```

---
&nbsp;

Now, refer to the [All OS](#all-os) section.

&nbsp;

### macOS

#### Xcode Command Line Tools  

Install Xcode Command Line Tools (this may take a while)

```shell
xcode-select --install
```

#### Docker Desktop on Mac

Install Docker Desktop following this [guide](https://docs.docker.com/desktop/install/mac-install/)

#### Ansible on Mac

Since Python 3 with pip is already installed you just have to install `ansible`

```shell
pip3 install ansible
```

Since `zsh` is the default shell in macOS, run the following command

```shell
echo "export PATH=\$PATH:$(python3 -m site --user-base)/bin" | tee -a ~/.zshrc
```

---
&nbsp;

Now, refer to the [All OS](#all-os) section.

&nbsp;

### Windows

On Windows, you can use the WSL 2 with Ubuntu

#### Install WSL/Ubuntu

Open a Terminal with administrator permissions and install the WSL with Ubuntu as default distribution

```shell
wsl --install -d Ubuntu
```

---

*Note*

You have to restart your system to complete the process!

---

#### Docker Desktop for Windows

Install Docker Desktop using (WSL 2 backend) following this [guide](https://docs.docker.com/desktop/install/windows-install/)

---

*Note*

You have to restart your system to complete the process!

---

#### Keychain

Due to some issues with detecting the correct ssh-key, the easiest way to bypass this on Windows WSL/Ubuntu is to use `keychain`.

Open the Ubuntu terminal and install `keychain`

```shell
sudo apt-get -y install keychain
```

Now, run the following command

```shell
echo "eval \$(keychain --eval id_rsa --quiet)" | tee -a ~/.bashrc
```

This will create a ssh-agent as soon as you start the Ubuntu terminal with your ssh key.
If you have created an ssh key with a passpharse, you have to unlock the ssh key every first start of the Ubuntu terminal.

---

*Note*

You have to restart your system after you completed the [All OS](#all-os) section below.

---

---
&nbsp;

Now, refer to the [All OS](#all-os) section.

&nbsp;

### All OS

#### SSH

---

*Note*

You can skip the generation of an ssh key if you already have an existing key!

---

Generate ssh key. For more security, you can specify a passphrase during the generation.

```ssh
ssh-keygen -t rsa -b 4096
```

Add the public ssh key of gitlab.com to the `known_hosts` file

```shell
ssh-keyscan gitlab.com | tee -a ~/.ssh/known_hosts
```

Print the public ssh key

```shell
cat ~/.ssh/id_rsa.pub
```

And ensure that you add the public key to the [SSH Keys sections](https://gitlab.com/-/profile/keys) in GitLab.

#### Git

Edit the global git config

```shell
git config --global -e
```

Ensure the following content (keep in mind, that the spaces in the lines are tabs)

```ini
[user]
	email = your.name@domain.tld
	name = Your Name
[core]
	autocrlf = input
[init]
	defaultBranch = main
[url "ssh://git@gitlab.com/"]
	insteadOf = https://gitlab.com/
```

## Setup on your own project

&nbsp;

Now, create these files with this content:

&nbsp;

### *ansible.cfg* - Ansible configuration

```ini
[defaults]
# Use the YAML callback plugin.
stdout_callback = yaml
# Use the stdout_callback when running ad-hoc commands.
bin_ansible_callbacks = true
# Detect python interpreter with not warnings.
interpreter_python = auto_silent
# Don't warn that host list is empty we always have the localhost available.
localhost_warning = false
# Don't show skipped messages
display_skipped_hosts = false

[inventory]
# Don't warn about inventory that can't be parsed, we don't have inventory.
inventory_unparsed_warning = false

[ssh_connection]
# ssh arguments to use; Will not add docker hosts.
ssh_args = -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no"
# enable pipelining for performance increase with docker container
pipelining = true
```

### *ansible.requirements.yml* - Ansible playbook dependencies

```yaml
---
collections:
  - name: https://gitlab.com/ideaplexus/ansible-dev-tools.git
    type: git
    version: main

```

### *ansible.dev.yml* - Ansible playbook

```yaml
# code: language=ansible
---
- name: Development
  hosts: localhost
  gather_facts: false
  remote_user: user
  vars:
    container:
      enabled: true # per default, container is enabled and preferred, but you can disable it by setting enabled to false

    project:
      name: example
      # type of project, could be "service", "lambda", "lambda/provided.al2" or "library"
      type: service
      languages:
        - go

    ## optional: enable this, if resources from the internal repository are required
    #go:
    #  vendor: 
    #    private: gitlab.com

  tasks:
    - name: Container development roles
      ansible.builtin.import_role:
        name: invia.development.container
    - name: Go development roles
      ansible.builtin.import_role:
        name: invia.development.go

```

### *ansible.ci.yml* - Ansible playbook - use with gitlab ci pipeline

```yaml
# code: language=ansible
---
- name: Development
  hosts: localhost
  gather_facts: false
  remote_user: user
  vars:
    project:
      name: example
      # type of project, could be "service", "lambda", "lambda/provided.al2" or "library"
      type: service
      languages:
        - go

    ## optional: enable this, if resources from the internal repository are required
    #go:
    #  vendor: 
    #    private: gitlab.com

  tasks:
    - name: Go development roles
      ansible.builtin.import_role:
        name: invia.development.go

```

### *Makefile* - Shortcuts for most common tags

```makefile
#!make

# Include .env, if exists
-include .env

# Set playbook based on environment
ifdef CI
PLAYBOOK := "ansible.ci.yml"
else
PLAYBOOK := "ansible.dev.yml"
endif

.PHONY: help
help: ## Show this help.
	@awk 'BEGIN {FS = ":.*##"; printf "Usage:\033[36m\033[0m\n"} /^[$$()% a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-17s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

.PHONY: init
init: ## Download the playbook dependencies.
	ansible-galaxy install -r ansible.requirements.yml

.PHONY: container
container: ## Force the rebuild of the containers.
	ansible-playbook $(PLAYBOOK) --tags "container:rebuild"

.PHONY: all
all: ## Run all targets.
	ansible-playbook $(PLAYBOOK)

.PHONY: clean
clean: ## Clean the project by removing vendor, build, lint and test output.
	ansible-playbook $(PLAYBOOK) --tags="clean"

.PHONY: check
check: ## Run lint and test, useful before committing changes.
	ansible-playbook $(PLAYBOOK) --tags="lint,test"

.PHONY: vendor
vendor: ## Install the dependencies.
	ansible-playbook $(PLAYBOOK) --tags="vendor"

.PHONY: vendor-upgrade
vendor-upgrade: ## Upgrade the dependencies.
	ansible-playbook $(PLAYBOOK) --tags="vendor:upgrade"

.PHONY: generate
generate: ## Run all configured generate tasks.
	ansible-playbook $(PLAYBOOK) --tags="generate"

.PHONY: fix
fix: ## Lint and fix the project.
	ansible-playbook $(PLAYBOOK) --tags="fix"

.PHONY: lint
lint: ## Lint the project.
	ansible-playbook $(PLAYBOOK) --tags="lint"

.PHONY: test
test: ## Test the project.
	ansible-playbook $(PLAYBOOK) --tags="test"

.PHONY: build
build: ## Build the binaries.
	ansible-playbook $(PLAYBOOK) --tags="build"
```

---

*Note*

Not all roles/tasks configured in the `ansible.dev.yml` are required during CI.<br/>
Therefore, the `ansible.ci.yml` only contains CI relevant configuration, e.g. no container role, no mockery tasks,...

## Usage

---

Install playbook dependencies with this command

```shell
ansible-galaxy install -r ansible.requirements.yml
```

To execute the playbook, run the following command

```shell
ansible-playbook ansible.dev.yml
```

## Advanced usage

Ansible is structured in roles with tasks to run. These can be executed by using the `--tags` with the role name or the name of the tags on the tasks.

### Roles

#### `common`

The `common` role holds several tasks to initialize the playbook. It is a dependency of all other roles.

#### `container`

The `container` role is a role to prepare the project to run other roles inside containers.

#### `container_ssh`

The `container_ssh` role includes tasks that create a docker container running an ssh agent.
It also creates a shared docker volume that contains the ssh socket so that ssh connections can be established within containers via this socket.
This is necessary for the checkout via git within a container.

| Variables                           | Default value                             | Type      | Description                                                                                                                                           |
| :---------------------------------- | :---------------------------------------- | :-------- | :---------------------------------------------------------------------------------------------------------------------------------------------------- |
| `go.version`                        | auto detected                             | `string`  | The go version which is used for the dev container. <br /> The version is detected by the `common` role from the `go.mod` file, but can be overridden |
| `container.enabled`                 | `true`                                    | `boolean` | Enables or disables the container support.                                                                                                            |
| `container.ssh_port`                | `2222`                                    | `integer` | Set the port that is used to connect to the containers.                                                                                               |
| `container.dev.extra_instructions`  | `-`                                       | `string`  | This allows to add extra instructions to the for the dev container                                                                                    |
| `container.dev.dockerfile_template` | `{{ role_path }}/templates/Dockerfile.j2` | `string`  | This allows to set a path to a different Dockerfile for the dev container.                                                                            |

```yaml
# ansible.*.yml
# [...]
  vars:
    container:
      enabled: false
      ssh_port: 22
      dev:
        extra_instructions: |-
          RUN set -eux \
            && apk add --no-cache \
              nodejs yarn
        dockerfile_template: dev.Dockerfile
# [...]
```

Also, container support can be enabled/disabled by .env entry. This will automatically loaded if the Makefile is used.

```dotenv
# .env
# [...]
ANSIBLE_CONTAINER_ENABLED=false
# [...]
```

This role has the following tags:

| Tags                    | Description                                                    |
| :---------------------- | :------------------------------------------------------------- |
| `container:rebuild`     | This run forces a rebuild of the ssh and dev docker container. |
| `container-ssh:rebuild` | This run forces a rebuild of the ssh docker container.         |
| `container-dev:rebuild` | This run forces a rebuild of the dev docker container.         |

#### `go`

The `go` role is a meta-role wich includes the following roles:

- `go_clean`
- `go_vendor`
- `go_oapi_codegen`
- `go_mockery`
- `go_generate`
- `go_golangci_lint`
- `go_test`
- `go_build`

#### `go_clean`

The `go_clean` role includes tasks to remove created files and folders:

- `cc.out`
- `co.out`
- `cp.html`
- `cp.out`
- `cs.out`
- `tr.out`
- `build`
- `vendor`

This role has the following tags:

| Tags       | Description                                                               |
| :--------- | :------------------------------------------------------------------------ |
| `go:clean` | Run all tasks in the `go_clean` role.                                     |
| `go:all`   | Run all tasks in the `go_clean` role. This tag may also runs other roles. |
| `clean`    | Run all tasks in the `go_clean` role. This tag may also runs other roles. |

#### `go_vendor`

The `go_vendor` role includes tasks to download or update go dependencies.

| Variables           | Default value | Type     | Description                        |
| :------------------ | :------------ | :------- | :--------------------------------- |
| `go.vendor.private` | `nil`         | `string` | Domain of a private git repository |
| `go.vendor.output`  | `false`       | `bool`   | Show output of tasks               |

This role has the following tags:

| Tags                | Description                                                                                                                     |
| :------------------ | :------------------------------------------------------------------------------------------------------------------------------ |
| `go:vendor`         | Run tasks in the `go_vendor` role.                                                                                              |
| `go:vendor:upgrade` | Run tasks in the `go_vendor` role. This runs an additional task to update the dependencies.                                     |
| `go:all`            | Run tasks in the `go_vendor` role. This tag may also runs other roles.                                                          |
| `vendor`            | Run tasks in the `go_vendor` role. This tag may also runs other roles.                                                          |
| `vendor:upgrade`    | Run tasks in the `go_vendor` role. This runs an additional task to update the dependencies. This tag may also runs other roles. |

#### `go_oapi_codegen`

The `go_oapi_codegen` role includes tasks that generate go files from openapi schema files with [`oapi-codegen`](https://github.com/deepmap/oapi-codegen).

| Variables                          | Default value | Type      | Description                                    |
| :--------------------------------- | :------------ | :-------- | :--------------------------------------------- |
| `go.oapi_codegen.enabled`          | `false`       | `boolean` | Enables or disables the oapi-codegen support.  |
| `go.oapi_codegen.version`          | `latest`      | `string`  | This overrides the version to install of file. |
| `go.oapi_codegen.schemas`          | `[]`          | `array`   | A list of schemas to generate.                 |
| `go.oapi_codegen.schemas[].path`   | `-`           | `string`  | Path to the openapi schema file.               |
| `go.oapi_codegen.schemas[].config` | `-`           | `string`  | Path to the config file.                       |

```yaml
# ansible.*.yml
# [...]
  vars:
    go:
      oapi_codegen:
        enabled: true
        version: v2.0.0
        schemas:
          - path: "api/schema/server-openapi.yml"
            config: "api/config/example-config.yml"
# [...]
```

##### Example configurations

```yaml
package: api
generate:
  models: true
  embedded-spec: true
  echo-server: true
compatibility:
  old-merge-schemas: true
output: internal/api/api.gen.go
```

```yaml
generate:
  models: true
  echo-server: true
output-options:
  include-tags:
    - login
output: internal/api/login.gen.go
```

This role has the following tags:

| Tags              | Description                                                                      |
| :---------------- | :------------------------------------------------------------------------------- |
| `go:oapi-codegen` | Run all tasks in the `go_oapi_codegen` role.                                     |
| `go:all`          | Run all tasks in the `go_oapi_codegen` role. This tag may also runs other roles. |
| `generate`        | Run all tasks in the `go_oapi_codegen` role. This tag may also runs other roles. |

#### `go_mockery`

The `go_mockery` role includes tasks that generates mock files with [`mockery`](https://github.com/vektra/mockery).

| Variables            | Default value | Type      | Description                                       |
| :------------------- | :------------ | :-------- | :------------------------------------------------ |
| `go.mockery.enabled` | `false`       | `boolean` | Enables or disables the mockery support.          |
| `go.mockery.version` | `latest`      | `string`  | This overrides the version to install of mockery. |

If the project contains a `.mockery.yaml`, mocks will be generated from this file and the `mocks` block will be ignored.

```yaml
# ansible.*.yml
# [...]
  vars:
    go:
      mockery:
        enabled: true
        version: v1.2.3
# [...]
```

This role has the following tags:

| Tags         | Description                                                                 |
| :----------- | :-------------------------------------------------------------------------- |
| `go:mockery` | Run all tasks in the `go_mockery` role.                                     |
| `go:all`     | Run all tasks in the `go_mockery` role. This tag may also runs other roles. |
| `generate`   | Run all tasks in the `go_mockery` role. This tag may also runs other roles. |

#### `go_generate`

The `go_generate` role includes tasks to call `go generate`.

| Variables                   | Default value | Type            | Description                                                  |
| :-------------------------- | :------------ | :-------------- | :----------------------------------------------------------- |
| `go.generate.enabled`       | `false`       | `boolean`       | Enables or disables the go generate support.                 |
| `go.generate.extra_go_bins` | `[]`          | `array[string]` | A list of go binaries to install before calling go generate. |
| `go.generate.paths`         | `[]`          | `array[string]` | A list of paths which should be used with go generate.       |

```yaml
# ansible.*.yml
# [...]
  vars:
    go:
      generate:
        enabled: true
        extra_go_bins:
          - github.com/swaggo/swag/cmd/swag@latest
        paths:
          - ./...
# [...]
```

This role has the following tags:

| Tags          | Description                                                                  |
| :------------ | :--------------------------------------------------------------------------- |
| `go:generate` | Run all tasks in the `go_generate` role.                                     |
| `go:all`      | Run all tasks in the `go_generate` role. This tag may also runs other roles. |
| `generate`    | Run all tasks in the `go_generate` role. This tag may also runs other roles. |

#### `go_golangci_lint`

The `go_golangci_lint` role includes tasks to lint or fix go code and generate a checkstyle file.

| Variables                         | Default value | Type      | Description                                                            |
| :-------------------------------- | :------------ | :-------- | :--------------------------------------------------------------------- |
| `go.lint.golangci_lint.version`   | `latest`      | `string`  | This overrides the version to install of golangci-lint.                |
| `go.lint.golangci_lint.exit_code` | `2`           | `integer` | This set the exit code for golangci-lint in case of of linting issues. |
| `go.lint.golangci_lint.timeout`   | `-`           | `string`  | This set the timeout for the golangci-lint execution (optional).       |

```yaml
# ansible.*.yml
# [...]
  vars:
    go:
      golangci_lint:
          exit_code: 0
          timeout: 5m
# [...]
```

This will also generate the `cs.out` file in the project root.

This role has the following tags:

| Tags      | Description                                                                       |
| :-------- | :-------------------------------------------------------------------------------- |
| `go:lint` | Run all tasks in the `go_golangci_lint` role.                                     |
| `go:all`  | Run all tasks in the `go_golangci_lint` role. This tag may also runs other roles. |
| `lint`    | Run all tasks in the `go_golangci_lint` role. Alias for `go:lint`.                |

#### `go_test`

The `go_test` role includes tasks to test the go code and generate coverprofile files.

| Variables                           | Default value         | Type            | Description                                                 |
| :---------------------------------- | :-------------------- | :-------------- | :---------------------------------------------------------- |
| `go.test.src_dirs`                  | `['internal', 'pkg']` | `array[string]` | Add additional source directories.                          |
| `go.test.goverreport.version`       | `latest`              | `string`        | This overrides the version to install of goverreport.       |
| `go.test.gocover_cobertura.version` | `latest`              | `string`        | This overrides the version to install of gocover-cobertura. |
| `go.test.flags`                     | `[]`                  | `array[string`  | Add additional flags for the `go test` tasks.               |

This will also generate the `cp.out`, `cp.html` and `tr.out` files.

This role has the following tags:

| Tags      | Description                                                              |
| :-------- | :----------------------------------------------------------------------- |
| `go:test` | Run all tasks in the `go_test` role.                                     |
| `go:all`  | Run all tasks in the `go_test` role. This tag may also runs other roles. |
| `test`    | Run all tasks in the `go_test` role. Alias for `go:test`.                |

#### `go_build`

The `go_build` role includes tasks to build go binaries. If the project is of type `lambda` or `lambda/provided.al2` the binary will also be zipped.
Any zipped binary will be removed afterward.

| Variables                      | Default value | Type            | Description                                                                                     |
| :----------------------------- | :------------ | :-------------- | :---------------------------------------------------------------------------------------------- |
| `project.type`                 | `nil`         | `string`        | Possible values: `service`, `lambda`, `library`.                                                |
| `go.build.dirs`                | `['cmd']`     | `array[string]` | Directories to look for files build. Setting this variable overrides existing values.           |
| `go.build.files`               | `[]`          | `array`         | Additional list of files to build.                                                              |
| `go.build.files[].src`         | `-`           | `string`        | Path to source file.                                                                            |
| `go.build.files[].dst`         | `-`           | `string`        | Destination path for binary.                                                                    |
| `go.build.files[].zip`         | `-`           | `string`        | Destination path for the zipped binary (optional).                                              |
| `go.build.files[].environment` | `-`           | `object`        | Environment variables for this file. Setting variables overrides existing variables. (optional) |
| `go.build.files[].flags`       | `-`           | `array[string]` | Additional list of build flags for this file. (optional)                                        |
| `go.build.environment`         | `{}`          | `object`        | Environment variables for build. Setting variables overrides existing variables. (optional)     |
| `go.build.flags`               | `[]`          | `array[string]` | Additional list of build flags (optional).                                                      |
| `go.build.ldflags`             | `[]`          | `array[string]` | Additional list of build ldflags (optional).                                                    |

The directories set in `dirs` require to have the go standard folder structure: `cmd/${BINARY_NAME}/main.go`.
The resulting binaries are build to: `build/${BINARY_NAME}`. To create a different location the `files` section can be used.
For a project of type `lambda/provided.al2` binaries are build to: `build/bootstrap`. The corresponding binary will be deleted, if a zip file will be created.

Based on the project type `environment`, `flags` and `ldflags` are set dynamically.  

| Project type          | Environment                                                                     | Flags                                         | Ldflags        |
| :-------------------- | :------------------------------------------------------------------------------ | :-------------------------------------------- | :------------- |
| `service`             | `{'CGO_ENABLED': '0', 'GOPATH': '$GOPATH'}`                                     | `['-mod vendor', '-v']`                       | `['-s', '-w']` |
| `lambda`              | `{'CGO_ENABLED': '0', 'GOPATH': '$GOPATH', 'GOOS': 'linux'}`                    | `['-mod vendor', '-v']`                       | `['-s', '-w']` |
| `lambda/provided.al2` | `{'CGO_ENABLED': '0', 'GOPATH': '$GOPATH', 'GOOS': 'linux', 'GOARCH': 'arm64'}` | `['-mod vendor', '-v', '-tags lambda.norpc']` | `['-s', '-w']` |

```yaml
# ansible.*.yml
# [...]
  vars:
    go:
      build:
        dirs:
          - tools
        files:
          - src: main.go
            dst: build/app
# [...]
```

This role has the following tags:

| Tags       | Description                                                               |
| :--------- | :------------------------------------------------------------------------ |
| `go:build` | Run all tasks in the `go_build` role.                                     |
| `go:all`   | Run all tasks in the `go_build` role. This tag may also runs other roles. |
| `build`    | Run all tasks in the `go_build` role. Alias for `go:build`.               |

#### `buf`

The `buf` role includes tasks to generate code from `.proto` files using [buf](https://buf.build/).

| Variables                    | Default value                                                                                                     | Type      | Description                                                                                                        |
| :--------------------------- | :---------------------------------------------------------------------------------------------------------------- | :-------- | :----------------------------------------------------------------------------------------------------------------- |
| `buf.enabled`                | `false`                                                                                                           | `boolean` | Enables or disables the buf support.                                                                               |
| `buf.version`                | `latest`                                                                                                          | `string`  | This overrides the version to install of buf.                                                                      |
| `buf.plugins`                | `['google.golang.org/protobuf/cmd/protoc-gen-go@latest', 'google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest']` | `string`  | The protoc plugins to install.                                                                                     |
| `buf.definitions[]`          | `[]`                                                                                                              | `array`   | A list of definitions to generate.                                                                                 |
| `buf.definitions[].path`     | `-`                                                                                                               | `string`  | Path to the proto definitions                                                                                      |
| `buf.definitions[].config`   | `-`                                                                                                               | `string`  | Path to the [config file](https://buf.build/docs/configuration/v1beta1/buf-yaml) `buf.yaml` (optional)             |
| `buf.definitions[].template` | `-`                                                                                                               | `string`  | Path to the [generate config file](https://buf.build/docs/configuration/v1/buf-gen-yaml) `buf.gen.yaml` (optional) |

```yaml
# ansible.*.yml
# [...]
  vars:
    buf:
      enabled: true
      version: v1.2.3
      definitions:
        - path: api/proto/client
          config: api/proto/buf.yaml
          template: api/proto/buf.gen.yaml
# [...]
```

#### `vacuum`

The `vacuum` role includes tasks to validate OpenAPI/Swagger schema files using [vacuum](https://github.com/daveshanley/vacuum).

| Variables                  | Default value | Type      | Description                                                                               |
| :------------------------- | :------------ | :-------- | :---------------------------------------------------------------------------------------- |
| `vacuum.enabled`           | `false`       | `boolean` | Enables or disables the vacuum support.                                                   |
| `vacuum.version`           | `latest`      | `string`  | This overrides the version to install of vacuum.                                          |
| `vacuum.schemas[]`         | `[]`          | `array`   | A list of schemas to validate.                                                            |
| `vacuum.schemas[].path`    | `-`           | `string`  | Path to the openapi/swagger schema file.                                                  |
| `vacuum.schemas[].ruleset` | `-`           | `string`  | Path to the [ruleset file](https://quobix.com/vacuum/rulesets/understanding/). (optional) |

```yaml
# ansible.*.yml
# [...]
  vars:
    vacuum:
      enabled: true
      version: v1.2.3
      schemas:
        - path: api/schema/client-openapi.yml
          ruleset: api/ruleset/default.yml
# [...]
```

## Advanced examples

### Upgrade projects go version

- Manually set the target go version

```yaml
# code: language=ansible
---
- name: Development
  hosts: localhost
  gather_facts: false
  remote_user: user
  vars:
    project:
      name: example
      type: service
      languages:
        - go

    go:
      version: 1.20 # target version

  tasks:
    - import_role:
        name: invia.development.container
    - import_role:
        name: invia.development.go
```

- Rebuild the container

```shell
ansible-playbook ansible.dev.yaml --tags "container-dev:rebuild"
```

- Run the command in the re-created container

```shell
docker exec -it docker-container-${PROJECT_NAME} go mod tidy -go=${GO_VERSION_TARGET} -compat=${GO_VERSION_CURRENT}
```

- Now, dependencies can be updated as usually with

```shell
ansible-playbook ansible.dev.yaml --tags "go:vendor"
```
